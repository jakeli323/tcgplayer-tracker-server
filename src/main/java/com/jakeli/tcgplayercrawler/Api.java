package com.jakeli.tcgplayercrawler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@RestController
public class Api {
    private static final boolean kIsConsiderCountDiff = true;//默认：true；分析：是否开启数量差筛选功能
    private static final boolean kIsConsiderScarce = true;//默认：true；分析：是否开启总数量阈值筛选功能。
    private static final String kCondition = "and";//默认：or；分析：or后者and
    //todo 加上数量增加的增加的跟踪 “用于卖卡”
    private static final int kCountDiffThreshold = -5;//    默认：-4；-10分析：xx小时之内，卖出数量大于一定才显示
    private static final int kShowCountThreshold = 200;//默认：70;150,分析：当前数量小于xxx才显示。
    private static final int kMaxInterval = 120;//默认48；分析：xx小时之内的时间间隔的数量差所有系列的这个在24h之内变
    //todo markmark!!!!!! 这里有问题，无论interval设置成多少，都不能筛选出真正的时间范围内的东西


    private final int kLimit = 100;
    private final int kRedTotalCount = 70;

    private final static Logger logger = LoggerFactory.getLogger(Api.class);

    private String getRedCountStr(int count){
        String ret = String.valueOf(count);
        if(count <= kRedTotalCount){
//                    diffCountStr = "<span style=\"color:red\">" + diffCountStr + "</span>";
            ret = "<label class=\"lessThanRed\">" + count + "</label>";
        }

        return ret;
    }

    private String getHtmlByJo(List<JSONObject> jo){
        try{
            StringBuilder sb = new StringBuilder();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            sb.append("update time  = " + new Date().getTime());
            String dateStr = sdf.format(new Date());
            sb.append("更新: " + dateStr);
            sb.append("</br></br></br>");
            for(int i = 0; i < jo.size(); i++){
                if(i >= kLimit) break;

                JSONObject o = jo.get(i);
                String key = o.getString("cardName") + " | " + o.getString("setName");

                sb.append(key);
                sb.append("</br>");
                JSONObject countDiffObj = o.getJSONObject("pricesCountDiff");
//                sb.append(countDiffObj.toString());
                sb.append("时间间隔：" + countDiffObj.getString("maxTimeSpan"));
                sb.append("</br>");
                int diffCountInt = Integer.valueOf(countDiffObj.getString("diffCount"));

                sb.append("减少数量：" + diffCountInt);
                sb.append("</br>");

                JSONArray beginEndObj = o.getJSONArray("pricesCount");
                JSONObject beginObj = beginEndObj.getJSONObject(0);
                JSONObject endObj = beginEndObj.getJSONObject(1);

//                sb.append(beginEndObj.toString());
                sb.append("到xxx日期：" + beginObj.getString("date") + "; 总数量：" + getRedCountStr(beginObj.getInt("count")));
                sb.append("</br>");
                sb.append("从xxx日期：" + endObj.getString("date") + "; 总数量：" + getRedCountStr(endObj.getInt("count")));

                sb.append("</br>");

                String priceStr = "";
                if(o.has("price")){
                    priceStr = o.getString("price");
                }

                if(!priceStr.isEmpty()){
                    sb.append("价格：" + priceStr);
                }else{
                    sb.append("价格：抽风没抓到");
                }

                sb.append("</br>");

                //todo 这里增加轨迹
                JSONArray arr = o.getJSONArray("history");
                if(arr.length() > 2){
                    for(int t = 0; t < arr.length() - 1; t++){
                        JSONObject hisItem = arr.getJSONObject(t);
                        String dateItem = hisItem.getString("date");
                        dateItem = dateItem.substring(5, 10);
                        int countItem = Integer.valueOf(hisItem.getString("count"));
                        String countItemStr = " (<label class=\"hisCount\">" + countItem + "</label>) ";
                        String trans = "";
//                        ret = "<label class=\"lessThanRed\">" + count + "</label>";
                        if(t < arr.length() - 1){
                            int nextCountItem = Integer.valueOf(arr.getJSONObject(t+1).getString("count"));
                            int itemDiff = nextCountItem - countItem;

                            String className = "decText";
                            if(itemDiff >= 0){
                                className="incText";
                            }

                            trans = " -- <label class=\"" + className + "\">" + itemDiff + "</label> --> ";

                        }
                        sb.append(dateItem + countItemStr + trans);
                    }
//                    sb.append()
                }


                sb.append("</br>----------------------------------------</br>");

            }
            return sb.toString();

        }catch(JSONException e){
            e.printStackTrace();
        }

        return null;

    }

    @PostMapping(value = "/getJson")
    public String getJson(){

//        List<JSONObject> jo = new Crawler().getAnalyzedData(kIsConsiderCountDiff, kIsConsiderScarce, kCondition, kCountDiffThreshold, kShowCountThreshold, kMaxInterval);
        List<JSONObject> jo = new Crawler().getAnalyzedData(kIsConsiderCountDiff, kIsConsiderScarce, kCondition, kCountDiffThreshold, kShowCountThreshold, kMaxInterval);

         return getHtmlByJo(jo);
    }

    @PostMapping(value = "/getThresholdJson")
    public String getThresholdJson(){

        List<JSONObject> jo = new Crawler().getAnalyzedData(false, kIsConsiderScarce, "or", kCountDiffThreshold, 50, kMaxInterval);

        return getHtmlByJo(jo);
    }

    @RequestMapping(method = RequestMethod.POST, value="/upload")
    public void acceptData(InputStream is) throws Exception{

        StringBuilder sb = new StringBuilder();
        String line;

        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while((line = br.readLine()) != null){
            sb.append(line);
        }
//        private static void stringToFile(String str, String filePath){


        Crawler.stringToFile(sb.toString(), "somejsondata.json");

    }





}