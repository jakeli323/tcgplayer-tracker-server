package com.jakeli.tcgplayercrawler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Crawler {

    private static String[] goldfishUrls = {
            "https://www.mtggoldfish.com/movers-details/paper/standard/winners/dod",
            "https://www.mtggoldfish.com/movers-details/paper/modern/winners/dod"};

    private static String tcgplayerUrl = "https://shop.tcgplayer.com/magic/product/show?ProductName=%s&newSearch=false&ProductType=All&IsProductNameExact=true";
    private static int inc = 0;
    private static int total = 0;
    private static final String kDataRootDir = "crawlered_data/";

    private static final long kWaitTimeShort = 10;
    private static final long kWaitTimeLong = 20;//todo markmark 这里设置成20
    private static final long kWaitTimeImplicit = 5;
    private static final int kLoginTryTimes = 10;

    private static final String[] kSetIgnoreKeywords = {"Prerelease"};
    private static final String[] kAnalyzeSetIgnores = {"Invocations", "Expeditions", "Judge", "Explorers of", "Masterpiece", "Promos", "Magic Game Night", "Magic Player Rewards", "Commander"};

    private static boolean isConsiderCountDiff = true;//默认：true；分析：是否开启数量差筛选功能
    private static boolean isConsiderScarce = true;//默认：true；分析：是否开启总数量阈值筛选功能。
    private static String condition = "and";//默认：or；分析：or后者and
    //todo 加上数量增加的增加的跟踪 “用于卖卡”
    private static int countDiffThreshold = -5;//默认：-4；-10分析：xx小时之内，卖出数量大于一定才显示
    private static int showCountThreshold = 200;//默认：70;150,分析：当前数量小于xxx才显示。
    private static int maxInterval = 120;//默认48；分析：xx小时之内的时间间隔的数量差所有系列的这个在24h之内变
    //todo markmark!!!!!! 这里有问题，无论interval设置成多少，都不能筛选出真正的时间范围内的东西
    private static int toWatchThreshold = 100;//默认100


    public List<JSONObject> getAnalyzedData(boolean isConsiderCountDiff, boolean isConsiderScarce, String condition, int countDiffThreshold, int showCountThreshold, int maxInterval){
        this.isConsiderCountDiff = isConsiderCountDiff;
        this.isConsiderScarce = isConsiderScarce;
        this.condition = condition;
        this.countDiffThreshold = countDiffThreshold;
        this.showCountThreshold = showCountThreshold;
        this.maxInterval = maxInterval;

        return analyze();
    }

    private static List<JSONObject> analyze(){
        String jsonStr = fileToString("somejsondata.json");
        System.out.println("---------------开始分析-------------");
        try{
            JSONObject root = new JSONObject(jsonStr);
            Iterator<String> keys = root.keys();
            JSONObject output = new JSONObject();
            //todo 这里还要做个差值对比，昨天，今天，差值，大于某个阈值的显示。判断两次爬取的时间，如果
            //todo 大于二十四小时，那么就。。。相减看看数量少了多少。 超过阈值的请显示
            //todo 判断如果超过48小时数量没有减少的话就删掉。从tocrawllist删掉。
//            List<String>  //解决同一个set有一个减了也要维护的问题。

            while(keys.hasNext()){
                boolean isShow = false;
                boolean isCountDiffReached = false;
                boolean isScarceReached = false;
                boolean isNotIgnored = true;
                String key = keys.next();
                String[] sarr = key.split("\\|");
                String cardName = sarr[0];
                System.out.println("card name = " + cardName);
                if(isContainIgnore(key, kAnalyzeSetIgnores)) isNotIgnored = false;
                JSONObject o = root.getJSONObject(key);
                System.out.println("ooo = " + o.toString());
                JSONArray arr =  o.getJSONArray("pricesCount");
                int count = arr.getJSONObject(arr.length() - 1).getInt("count");
                String date = arr.getJSONObject(arr.length() - 1).getString("date");

//                String history = "";
//                for(int i = 0; i < arr.length() - 1; i++){
//                    arr.getJSONObject(i).getString()
//                }
                o.put("history", arr);



                if(arr.length() > 2){
                    int begin = arr.length() - 2;
                    boolean isMaxIntervalReached = false;
                    while(begin >= 0){
                        String lastDate = arr.getJSONObject(begin).getString("date");

                        //这里应该一直往前找，找到尽头，相差24小时以内时间间隔最大的两个的的数量。
                        //todo 有数量差才算时间差

                        long minuteDiff = calMinuteDiff(date, lastDate);
                        long hourDiff = minuteDiff / 60;
                        if(hourDiff > maxInterval){
                            isMaxIntervalReached = true;
                            break;
                        }
                        begin--;
                    }

                    if(begin < 0) begin = 0;

                    int lastCount = arr.getJSONObject(begin).getInt("count");
                    String lastDate = arr.getJSONObject(begin).getString("date");

                    long minuteDiff = calMinuteDiff(date, lastDate);
                    long hourDiff = minuteDiff / 60;
                    String toShowDiffStr = minuteDiff + "min";
                    if(minuteDiff > 60){
                        toShowDiffStr = hourDiff + "h";
                        if(hourDiff > 24){
                            int days = (int)hourDiff / 24;
                            int rest = (int)hourDiff % 24;
                            toShowDiffStr = days + "d" + rest + "h";
                        }
                    }

                    int countDiff = count - lastCount;

                    String hourDiffStr = toShowDiffStr;
                    JSONObject dateObj = new JSONObject();
                    dateObj.put("maxTimeSpan", hourDiffStr);
                    dateObj.put("diffCount", countDiff);
                    o.put("pricesCountDiff", dateObj);

                    if(countDiff <= countDiffThreshold && isConsiderCountDiff){
                        isCountDiffReached = true;
                    }



                    System.out.println("----------- end-------------");

                    arr = new JSONArray();
                    JSONObject beginObj = new JSONObject();
                    JSONObject endObj = new JSONObject();
                    beginObj.put("date", date);
                    beginObj.put("count", count);
                    endObj.put("date", lastDate);
                    endObj.put("count", lastCount);
                    arr.put(beginObj);
                    arr.put(endObj);

                    o.put("pricesCount", arr);
                }

                //todo markmark
                //没有remove掉的卡，加上tracked
                //铁牌干掉。爬的时候就。
                //这里还要把数量很少的卡加入另外一个结构并且存起来一个data文件。


                if(count < showCountThreshold && isConsiderScarce){
                    isScarceReached = true;
                    System.out.println("is carce reaced  true");
                }

                if(condition.equals("and") && isConsiderCountDiff && isConsiderScarce){
                    isShow = isScarceReached && isCountDiffReached && isNotIgnored;
                }else{
                    isShow = (isScarceReached || isCountDiffReached) && isNotIgnored;
                }

                if(isShow){
                    output.put(key, o);
                }


                //todo 这里把价格最新的两个拿出来看看日期，如果不同就拿之前的判断
                //todo 不过有可能半天就差距巨大了，这样吧，就拿最近两次爬取的数量差吧。
                //todo 最新两次爬取的数量差。譬如今天很多牌都变化，那么就半天爬一次我看看对比一下。
                ///todo 但是时间短了阈值就不同了才行
            }

            System.out.println("总共筛选出的entry数量：" + output.length());
            System.out.println("---------------分析结束-------------");


            //todo 这里筛选diff最大的30个
            int arrSize = output.length();

            List<JSONObject> objList = new ArrayList<>();
            Iterator it = output.keys();
            int c = 0;
            while(it.hasNext()){
                String keyEntry = (String)it.next();
                JSONObject jo = output.getJSONObject(keyEntry);

                if(jo != null){
                    if(jo.has("pricesCountDiff")){
                        if(jo.getJSONObject("pricesCountDiff").has("diffCount")){
                            objList.add(jo);
                        }
                    }
                }
            }

//            JSONObject[] objArr = (JSONObject)objList.to

            if(objList.size() > 0){
                objList.sort(new Comparator<JSONObject>() {
                    @Override
                    public int compare(JSONObject a, JSONObject b){
                        try{
                            int aCount = a.getJSONObject("pricesCountDiff").getInt("diffCount");
                            int bCount = b.getJSONObject("pricesCountDiff").getInt("diffCount");
                            if(aCount > bCount){
                                return 1;
                            }else if(aCount < bCount){
                                return -1;
                            }
                            return 0;
                        }catch(JSONException e){
                            e.printStackTrace();
                        }

                        return 0;
                    }
                });
            }


            return objList;

        }catch(JSONException e){
            e.printStackTrace();
        }

        return null;
    }

    public static String fileToString(String fileName){
        try{
            File tocrawlfile = new File(kDataRootDir + fileName);
            tocrawlfile.createNewFile();
            FileInputStream fin = new FileInputStream(tocrawlfile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine())!= null){
                sb.append(line);
            }
            reader.close();
            fin.close();

            String toCrawlListStr = sb.toString();
            return toCrawlListStr;
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;

    }

    public static void stringToFile(String str, String filePath){
        try{
            File file = new File(kDataRootDir + filePath);
            FileWriter writer = new FileWriter(file);
            writer.write(str);
            writer.flush();
            writer.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }



    private static void addPriceCount(JSONArray priceArr, int count){
        try{
            JSONObject po = new JSONObject();
            po.put("date", new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()));
            po.put("count", count);
            priceArr.put(po);
        }catch(JSONException e){
            e.printStackTrace();
        }

    }

    private static JSONObject getEntry(JSONObject root, String cardName, String setName){
        try{
            String key = cardName + "|" + setName;
            if(root.has(key)){
                return root.getJSONObject(key);
            }
        }catch(JSONException e){
            e.printStackTrace();
        }

        return null;
    }
    //                        if(isContainIgnore(setName)) continue;
    private static boolean isContainIgnore(String setName, String[] ignores){
        boolean ret = false;
        for(String keyStr : ignores){
            if(setName.contains(keyStr)){
                ret = true;
                break;
            }
        }

        return ret;
    }
    private static long calMinuteDiff(String date, String lastDate){
        try{
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            Date newDate = f.parse(date);
            Date oldDate = f.parse(lastDate);
            long timeDiff = newDate.getTime() - oldDate.getTime();
            long minuteDiff = timeDiff / 1000 / 60 ;
            return minuteDiff;
        }catch(ParseException e){
            e.printStackTrace();
        }

        return 0;

    }


    private static List<String> getToCrawlList(){
        String toCrawlListStr = fileToString("tocrawllist.data");
        String[] splitStr = toCrawlListStr.split(";");

        if(splitStr.length > 1){
            List<String> toCrawlCardListTemp =  Arrays.asList(splitStr);
            return  new ArrayList<>(toCrawlCardListTemp);
        }
        return null;
    }



    private static String getStringFromToCrawlCardList(List<String> toCrawlCardList){
        StringBuilder sbbuild = new StringBuilder();
        for(int i = 0; i < toCrawlCardList.size(); i++){
            sbbuild.append(toCrawlCardList.get(i));
            if(i != toCrawlCardList.size() - 1){
                sbbuild.append(";");
            }
        }

        return sbbuild.toString();
    }


}
